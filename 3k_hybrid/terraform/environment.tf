module "gitlab_ref_arch_gcp" {
  source = "../../modules/gitlab_ref_arch_gcp"

  prefix = var.prefix
  project = var.project

   # 3k Hybrid - k8s Node Pools
  webservice_node_pool_count = 2
  webservice_node_pool_machine_type = "n1-highcpu-16"

  sidekiq_node_pool_count = 3
  sidekiq_node_pool_machine_type = "n1-standard-4"

  supporting_node_pool_count = 2
  supporting_node_pool_machine_type = "n1-standard-2"

  # 3k
  consul_node_count = 3
  consul_machine_type = "n1-highcpu-2"

  gitaly_node_count = 3
  gitaly_machine_type = "n1-standard-4"
  
  praefect_node_count = 3
  praefect_machine_type = "n1-highcpu-2"

  praefect_postgres_node_count = 1
  praefect_postgres_machine_type = "n1-highcpu-2"

  gitlab_nfs_node_count = 1
  gitlab_nfs_machine_type = "n1-highcpu-4"

  haproxy_internal_node_count = 1
  haproxy_internal_machine_type = "n1-highcpu-2"

  monitor_node_count = 1
  monitor_machine_type = "n1-highcpu-2"

  pgbouncer_node_count = 3
  pgbouncer_machine_type = "n1-highcpu-2"

  postgres_node_count = 3
  postgres_machine_type = "n1-standard-2"

  redis_node_count = 3
  redis_machine_type = "n1-standard-2"
}

output "gitlab_ref_arch_gcp" {
  value = module.gitlab_ref_arch_gcp
}
