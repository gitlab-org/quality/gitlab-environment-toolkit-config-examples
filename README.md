# GitLab Environment Toolkit Config Examples

Configuration files example for managing the environment with the [GitLab Environment Toolkit](https://gitlab.com/gitlab-org/quality/gitlab-environment-toolkit).

Using the config with the Toolkit can be done by copying in the respective tools config to its designated folder in the Toolkit:

- `<ENV>/terraform` to `terraform/environments/<ENV>`
- `<ENV>/ansible` to `ansible/environments/<ENV>`

:exclamation: **Note:** Configurations are provided as references. Before provisioning the environment please check the latest [GitLab Environment Toolkit](https://gitlab.com/gitlab-org/quality/gitlab-environment-toolkit) documentation and ensure that your configuration corresponds with it as well as with the [Reference Architecture](https://docs.gitlab.com/ee/administration/reference_architectures/) requirements for this specific environment.

For any other questions or queries please raise an issue the [GitLab Environment Toolkit](https://gitlab.com/gitlab-org/quality/gitlab-environment-toolkit) project.
